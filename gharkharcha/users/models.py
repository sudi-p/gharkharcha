# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=100, blank=False)
    last_name = models.CharField(max_length=100, blank=False)
    email = models.EmailField(max_length=100, blank=False)
    phone = models.CharField(max_length=20, default='')
    password = models.CharField(max_length= 20, blank= False)

    def get_Absolute_url(self):
        return reverse ('/login')

    def __str__(self):
        return self.first_name
