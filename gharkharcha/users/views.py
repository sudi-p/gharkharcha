# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import User

# Create your views here.

def index(request):
    return HttpResponse('This is the mf index page.')

def signup(request):
    if(request.method == 'GET'):
        return render(request, 'signup.html')

    else:
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        phone = request.POST['phone']
        password = request.POST['password']
        new_user = User(
        first_name = first_name,
        last_name = last_name,
        email = email,
        phone = phone,
        password = password,
        )
        new_user.save()
        return render(request, 'login.html')

class userCreate(CreateView):
    model = User
    fields = ['first_name', 'last_name','email','phone','password']

def login(request):
    if (request.method == 'GET'):
        return render(request, 'login.html')

    else:
        try:
            username = request.POST['username']
            password = request.POST['password']
            logged_user = User.objects.get(
                Q(email = username) | Q(phone =username)
            )
            if (logged_user):
                if(logged_user.password == password):
                    request.session['user'] = username
                    return HttpResponseRedirect("/dashboard")
                else:
                    return HttpResponse("password bigryo ni ta babu" )
            else:
                return HttpResponse("user bhetiyena")
        except Exception as e:
            return render(request, 'login.html')

def dashboard(request):
    context={
        'user' : request.session['user']
    }
    return render(request, 'dashboard.html', context)

def logout(request):
    request.session.clear()
    return HttpResponseRedirect("/login")
