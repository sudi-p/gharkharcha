# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .models import Transactions
from django.views import generic

# Create your views here.

def add(request):
    if (request.method == "POST"):
        user = request.session['user']
        item = request.POST['item']
        cost = request.POST['cost']
        transcation = Transactions(
            user= user,
            item = item,
            cost =cost,
        )
        transcation.save()
        return HttpResponseRedirect('/dashboard')

    else:
        return render(request, 'add.html')


class MyBillView(generic.ListView):
    template_name = 'mytransactions.html'
    context_object_name = 'transactions'
    def get_queryset(self):
        user = self.request.session['user']
        return Transactions.objects.filter(user = user)

class SingleTransactionView(generic.DetailView):
    model = Transactions
    context_object_name = 'bill'
    template_name = 'singletransaction.html'

    def get_object(self):
        try:
            user = self.request.session['user']
            self.object = Transactions.objects.get(user = user)
        except Transactions.DoesNotExist:
            raise Http404("transaction doesnot exist")

        return self.object

def singlebill(request, id):
    try:
        user = request.session['user']
        bill = Transactions.objects.get(id = id, user=user)
    except Transactions.DoesNotExist:
        raise Http404("transaction doesnot exist")

    return render(request, 'singletransaction.html', {'bill': bill})

class AllTransactionsView(generic.ListView):
    template_name = 'alltransactions.html'
    context_object_name = 'transactions'
    def get_queryset(self):
        return Transactions.objects.all()

def pay_bill(request):
    return HttpResponse('pay my bill')
