from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^add/', views.add, name='add'),
    url(r'^mybill/', views.MyBillView.as_view(), name='my_bill'),
    url(r'^allbill/', views.AllTransactionsView.as_view(), name='all_bill'),
    url(r'^(?P<pk>[0-9]+)/$', views.SingleTransactionView.as_view(), name='single_bill'),
    #url(r'^(?P<id>[0-9]+)/$', views.singlebill, name='single_bill'),
]
