# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Transactions(models.Model):
    user = models.CharField(max_length = 100, blank=False)
    item = models.CharField(max_length = 100, blank=False)
    cost = models.CharField(max_length = 100, blank=False)
    def __str__(self):
        return self.user
