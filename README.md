# gharkharcha

# 1> First of all, create a virtual environmant
	-virualenv env

# 2> Activte the virtual environment
	-source env/bin/activate
	
# 3> install the required modules from requirements.txt
	-pip install -r requirements.txt
	
# 4> go to the location where manage.py exists and start the server and You are good to go.
	- cd gharkharcha
	-python manage.py runserver
	
# 5> create database and run migrations(see setting.py in gharkharcha/gharkharcha folder for database information) and run server again
	-python manage.py makemigrations
	-python manage.py migrate
	-python manage.py runserver